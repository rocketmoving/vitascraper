# Import libraries for Selenium
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

# Import libraries for openpyxl
import openpyxl

# Import libraries
from time import sleep
import os

# Set global variables
chromedriver_path = r'C:\Projects\chromedriver_win32\chromedriver.exe'
browser = webdriver.Chrome(chromedriver_path)


def get_bsn_products():
    """Function to extract details for BSN products."""
    # Clear the terminal
    os.system('cls')
    # Print welcome message
    print(f'Welcome to the vitascraper\n')
    sleep(3)

    def get_bsn_target_list():
        """Get excel with product links."""
        # Define the path to the excel file
        target_excel = r'C:\Projects\vitascraper\bsn\bsn_website_urls.xlsx'

        # Open the Excel workbook
        wb = openpyxl.load_workbook(target_excel)
        sheet = wb.active
        last_link = sheet.max_row
        print(f'Using links up to row {last_link}')

        # Start a counter
        counter = 1
        # Create a list for webpage urls
        bsn_urls = []
        # Get each link
        for row in sheet.iter_rows():
            # Webpage link
            webpage_url = sheet['A'+str(counter)].value
            if webpage_url:
                #print(f'Adding link to BSN list: {webpage_url}')
                bsn_urls.append(webpage_url)
            counter += 1

        print(f'Searching through these urls: {bsn_urls}\n')
        sleep(3)
        return bsn_urls
    
    # Get the list of BSN targes
    target_urls = get_bsn_target_list()


    def get_bsn_target_webpage(target_url, idx):
        """Function to get the webpage for each url."""
        # Get the BSN webpage
        product_page = browser.get(target_url)
        sleep(3)
        # Get the right block section
        right_block = browser.find_element_by_class_name('block-region-top-right')
        # Get the title
        product_title = right_block.find_element_by_tag_name('h1').text
        # Replace any line breaks
        product_title = product_title.replace('\n', ' ')
        # Get the title sub line
        product_title_sub = right_block.find_element_by_xpath('/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/section[1]/div[1]/div[1]/div[3]/article[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[3]/div[1]').text
        # Get the description
        product_description = browser.find_element_by_class_name('product-overview').get_attribute('innerHTML')
        # Remove divs
        product_description = product_description.replace('<div class="product-keys">\n', '')
        product_description = product_description.replace('</div>\n', '')
        # Remove spans
        product_description = product_description.replace('<span>', '')
        product_description = product_description.replace('</span>', '')
        # Remove labels
        product_description = product_description.replace('<label>', '')
        product_description = product_description.replace('</label>', '')

        # Check if the product has flavors
        try:
            flavors = right_block.find_element_by_id('edit-configurables-variantsubflavour')
            if flavors:
                has_flavors = True
            else:
                has_flavors = False
        except:
            has_flavors = False        

        # Print a summary
        print(f'#{idx+1}\nTitle: {product_title}\nTitle sub:{product_title_sub}')
        print(f'Has flavors: {has_flavors}')
        print(f'Description: {product_description}')
        input('hold')





        # # Check if the product has flavors
        # try:
        #     flavors = right_block.find_element_by_id('edit-configurables-variantsubflavour')
        #     if flavors:
        #         has_flavors = True
        #         options = flavors.find_elements_by_tag_name('option')
        #         for option in options:
        #             option.click()
        #             print(f'Found a flavor: {option.text}')
        #             sleep(3)
        #             # Check each size










        # # Check if the product has sizes
        # try:
        #     sizes = right_block.find_element_by_id('edit-configurables-productsize')
        #     if sizes:
        #         has_sizes = True
        #     else:
        #         has_sizes = False
        # except:
        #     has_sizes = False        



        # Get the MSRP
        #product_msrp = right_block.find_element_by_class_name('acq-commerce-price').text
        
        print('\n')

    # Clear the terminal
    os.system('cls')
    for idx, target_url in enumerate(target_urls):
        get_bsn_target_webpage(target_url=target_url, idx=idx)





get_bsn_products()