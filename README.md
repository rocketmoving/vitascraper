#Vitascraper

**A bot to scrape vitamin and supplement product information from websites.**

Written in Python 🐍

Verify websites' terms of use. Use at your own risk.

If you've enjoyed this content, give us a shoutout on Instagram: @rocketmovingapp

*View more content on our [Rocket Moving App development blog](https://www.rocketmoving.dev).*

---

## Requirements

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).


**1. Use a virtual environment (optional)**

Recommended but not required.

*Windows: Run 'python -m venv venv'*


**2. Install openpyxl**

Used to read Excel files.

*Windows: Run 'pip install openpyxl'*


**3. Install selenium**

Used to launch Chromedriver. The bot uses a Chrome browser to extract details.

*Windows: Run 'pip install selenium'* 

---

## Code Layout

**Structure**

Each 'brand' has it's own folder within the main directory. Use each brand's folder to hold:

* Any related excel tables
* Folder for image files
* Folder for text files

**Files**

* main.py

*The main script that runs the bot.* 

--

## Additional Resources

* Openpyxl
Check their webpage for official [docs](https://openpyxl.readthedocs.io/en/stable/) [https://openpyxl.readthedocs.io/en/stable/](https://openpyxl.readthedocs.io/en/stable/).

* Selenium
Check their webpage for official [docs](https://www.seleniumhq.org/) [https://www.seleniumhq.org/](https://www.seleniumhq.org/).

